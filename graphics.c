#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include "graphics.h"
#include "main.h"

//A LARGE PART OF THIS FILE IS COPIED AND ADAPTED FROM THE EXAMPLE PROVIDED IN MINERVA

SDL_Window *window = NULL; //display window

void initialiseGrid(int x, int y) //sets up the window for display
{
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
    fprintf(stderr, "SDL_Init: %s\n", SDL_GetError()); //error catching

  atexit(SDL_Quit); //set for clean-up on exit

  SDL_CreateWindowAndRenderer(usedY*TILE_SIZE, usedX*TILE_SIZE, 0, &window, &renderer); //window size set to match size of current grid
  SDL_SetWindowTitle(window, "Game of Life"); //title of window
  SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
  SDL_RenderClear(renderer);
  SDL_RenderPresent(renderer);

  //get the images stored
  live_surface = SDL_LoadBMP("../images/liveCell.bmp");
  dead_surface = SDL_LoadBMP("../images/deadCell.bmp");

  live_texture = SDL_CreateTextureFromSurface(renderer, live_surface);
  dead_texture = SDL_CreateTextureFromSurface(renderer, dead_surface);

  SDL_RenderPresent(renderer);
}

void updateGrid()
{
  //single cell display
  SDL_Rect rect;

  rect.h = TILE_SIZE;
  rect.w = TILE_SIZE;

  //go through every cell in the grid
  for(int i=0; i<usedX; i++)
  {
    for(int j=0; j<usedY; j++)
    {
      rect.y = i*TILE_SIZE;
      rect.x = j*TILE_SIZE;
      if(grid[i][j]==1) SDL_RenderCopy(renderer, live_texture, NULL, &rect); //if its alive --- give it the alive texture
      else SDL_RenderCopy(renderer, dead_texture, NULL, &rect); //if its dead --- give it the dead texture
    }
  }

  SDL_RenderPresent(renderer);
}
