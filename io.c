#include <stdio.h>
#include "io.h"
#include "main.h"

/*file format as follows:
[X rows for usage up to MaxX][Y columns for usage up to maxY]
[0s and 1s to match previous line]

Files are always assumed to be in correct format
Initial state creation is not done by the user -- therefore, it can be assumed
that the provided files are always correct
*/

//filename must have a .bin file extension
int loadStartState(char filename[]) //loads a state into the game
{
  FILE *f;

  f = fopen(filename, "ab"); //file is first opened as append and nothing is done just to create it if it doesn't exist
  fclose(f);

  f = fopen(filename, "rb"); //file is opened for reading

  if(f==NULL) return 0; //if file cannot be opened, return error message
  else
  {
    fseek(f, 0, SEEK_SET); //jump to beginning (just in case)
    //read the grid dimensions
    fread(&usedX, sizeof(int), 1, f);
    fread(&usedY, sizeof(int), 1, f);
    if(usedX < 0 || usedY < 0 || usedX > MaxX || usedY > MaxY) return 0; //if invalid dimensions read
    int readGrid[MaxX][MaxY]; //2d array for reading
    fread(&readGrid, sizeof(int), MaxX*MaxY, f); //read it from file
    fclose(f); //file not needed anymore - close it
    //copy the grid into the program
    for(int i=0; i<usedX; i++)
    {
      for(int j=0; j<usedY; j++)
      {
        grid[i][j] = readGrid[i][j];
        if(grid[i][j] != 0 && grid[i][j] != 1) return 0; //if any of the cells are invalid --- return error
      }
    }
    return 1;
  }
}

int saveFinalState(char filename[]) //saves the end state into a file
{
  FILE *f;
  f = fopen(filename, "wb"); //file opened for writing

  if(f==NULL) return 0; //if file cannot be opened, return error message
  else
  {
    //write in the grid dimensions
    fwrite(&usedX, sizeof(int), 1, f);
    fwrite(&usedY, sizeof(int), 1, f);
    //write in the grid
    fwrite(&grid, sizeof(int), MaxX*MaxY, f);
    //close file
    fclose(f);
    return 1;
  }
}

//THIS IS FOR CREATING STARTING STATES OF THE GAME (NEVER ACCESSED BY USER UNLESS CODE EDITED)
int createState() //creates new state
{
  FILE *f;

  //input desired file name here (with a .bin file extension)
  f = fopen("testState.bin", "wb");

  if(f==NULL) return 0;
	else
	{
    //creating what to input to file -- can be done manually or not
    //world dimensions (greater than 0, lower than maximal allowed):
    int x = 20;
    int y = 20;
		fwrite(&x, sizeof(int), 1, f);
    fwrite(&y, sizeof(int), 1, f);

    //initial grid setup (only include 1s for alive cells and 0s for dead cells):
    //usage: grid[chosen_y][chosen_x] = (1 or 0)

    //first all cells are set to 0
    for(int i=0; i<x; i++)
    {
      for(int j=0; j<y; j++)
      {
        grid[i][j] = 0;
      }
    }
    //choose what cells to make alive here:
    grid[10][10]=1;
    grid[12][10]=1;
    grid[12][9]=1;
    grid[11][12]=1;
    grid[12][13]=1;
    grid[12][14]=1;
    grid[12][15]=1;
    //write the grid into the file
    fwrite(&grid, sizeof(int), MaxX*MaxY, f);
  }
}
