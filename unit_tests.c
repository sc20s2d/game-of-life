#include "unity.h"
#include "main.h"
#include "io.h"

//graphics functions cannot be tested as they have no return values (void functions) and they only provide an appearance for other already tested functions
//function tested can be read from the function names here (test + original function name)

void testDisplayOptions()
{
  const char* expected = "\nPlease choose an option:\n1) Continue playing the game\n2) Exit the game (current state of the game will be saved)\n>>> ";
  const char* actualChar = displayOptions();
  TEST_ASSERT_EQUAL_STRING(expected, actualChar);
}

void testDisplayStepOptions()
{
  const char* expected = "\nHow many states should the game go through?\nPlease enter your choice:\n>>> ";
  const char* actualChar = displayStepOptions();
  TEST_ASSERT_EQUAL_STRING(expected, actualChar);
}

void testUserChoice()
{
  int actual = userChoice("1");
  TEST_ASSERT_EQUAL_INT(1, actual);
  actual = userChoice("2");
  TEST_ASSERT_EQUAL_INT(2, actual);
  actual = userChoice("6");
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = userChoice("a");
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = userChoice("asdfghjkl");
  TEST_ASSERT_EQUAL_INT(0, actual);
}

void testUserStepChoice()
{
  int actual = userStepChoice("1");
  TEST_ASSERT_EQUAL_INT(1, actual);
  actual = userStepChoice("100");
  TEST_ASSERT_EQUAL_INT(100, actual);
  actual = userStepChoice("101");
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = userStepChoice("0");
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = userStepChoice("a");
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = userStepChoice("asdfghjkl");
  TEST_ASSERT_EQUAL_INT(0, actual);
}

void testLoadStartState()
{
  int loadTest = loadStartState("testState1.bin");
  TEST_ASSERT_EQUAL_INT(1, loadTest);
  TEST_ASSERT_EQUAL_INT(0, grid[0][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[0][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][2]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][2]);
  loadTest = loadStartState("testState2.bin");
  TEST_ASSERT_EQUAL_INT(1, loadTest);
  TEST_ASSERT_EQUAL_INT(1, grid[0][0]);
  loadTest = loadStartState("testState3.bin");
  TEST_ASSERT_EQUAL_INT(1, loadTest);
  TEST_ASSERT_EQUAL_INT(1, grid[0][0]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][2]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][4]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][2]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][4]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][0]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][1]);
  TEST_ASSERT_EQUAL_INT(1, grid[2][2]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][4]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][0]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][2]);
  TEST_ASSERT_EQUAL_INT(1, grid[3][3]);
  TEST_ASSERT_EQUAL_INT(1, grid[3][4]);
  loadTest = loadStartState("testState4.bin");
  TEST_ASSERT_EQUAL_INT(0, loadTest);
  loadTest = loadStartState("testState5.bin");
  TEST_ASSERT_EQUAL_INT(0, loadTest);
}

void testSaveFinalState()
{
  //first prepare a set up grid
  int testState = loadStartState("testState1.bin");
  if(testState == 0) TEST_FAIL();
  //save the grid
  testState = saveFinalState("finalStateTest1.bin");
  if(testState == 0) TEST_FAIL();
  //reload the grid
  testState = loadStartState("finalStateTest1.bin");
  if(testState == 0) TEST_FAIL();
  //check if everything was saved and then loaded correctly
  TEST_ASSERT_EQUAL_INT(1, testState);
  TEST_ASSERT_EQUAL_INT(0, grid[0][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[0][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][2]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][2]);
}

void testDetermineCell()
{
  int actual = determineCell(0,0);
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = determineCell(0,1);
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = determineCell(0,0);
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = determineCell(2,0);
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = determineCell(2,1);
  TEST_ASSERT_EQUAL_INT(1, actual);
  actual = determineCell(3,0);
  TEST_ASSERT_EQUAL_INT(1, actual);
  actual = determineCell(3,1);
  TEST_ASSERT_EQUAL_INT(1, actual);
  actual = determineCell(4,0);
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = determineCell(4,1);
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = determineCell(8,0);
  TEST_ASSERT_EQUAL_INT(0, actual);
  actual = determineCell(8,1);
  TEST_ASSERT_EQUAL_INT(0, actual);
}

void testProcessStep()
{
  //load a grid to process
  int testState = loadStartState("testState1.bin");
  if(testState == 0) TEST_FAIL();
  //process the grid
  int testStep = processStep();
  //test if could process
  TEST_ASSERT_EQUAL_INT(1, testStep);
  //test if step was processed correctly
  TEST_ASSERT_EQUAL_INT(1, grid[0][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[0][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][2]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][2]);

  //load a grid to process
  testState = loadStartState("testState2.bin");
  if(testState == 0) TEST_FAIL();
  //process the grid
  testStep = processStep();
  //test if could process
  TEST_ASSERT_EQUAL_INT(1, testStep);
  //test if step was processed correctly
  TEST_ASSERT_EQUAL_INT(0, grid[0][0]);

  //load a grid to process
  testState = loadStartState("testState3.bin");
  if(testState == 0) TEST_FAIL();
  //process the grid
  testStep = processStep();
  //test if could process
  TEST_ASSERT_EQUAL_INT(1, testStep);
  //test if step was processed correctly
  TEST_ASSERT_EQUAL_INT(0, grid[0][0]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][2]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][4]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][2]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][4]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][0]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][1]);
  TEST_ASSERT_EQUAL_INT(1, grid[2][2]);
  TEST_ASSERT_EQUAL_INT(1, grid[2][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][4]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][0]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][2]);
  TEST_ASSERT_EQUAL_INT(1, grid[3][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][4]);
}

void testStepTimer()
{
  //load a grid to process
  int testState = loadStartState("testState1.bin");
  if(testState == 0) TEST_FAIL();
  //process the grid
  int testTimer = stepTimer(5);
  //test if could process
  TEST_ASSERT_EQUAL_INT(1, testTimer);
  //test if step was processed correctly
  TEST_ASSERT_EQUAL_INT(1, grid[0][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[0][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][2]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[1][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][2]);

  //load a grid to process
  testState = loadStartState("testState2.bin");
  if(testState == 0) TEST_FAIL();
  //process the grid
  testTimer = stepTimer(5);
  //test if could process
  TEST_ASSERT_EQUAL_INT(1, testTimer);
  //test if step was processed correctly
  TEST_ASSERT_EQUAL_INT(0, grid[0][0]);

  //load a grid to process
  testState = loadStartState("testState3.bin");
  if(testState == 0) TEST_FAIL();
  //process the grid
  testTimer = stepTimer(5);
  //test if could process
  TEST_ASSERT_EQUAL_INT(1, testTimer);
  //test if step was processed correctly
  TEST_ASSERT_EQUAL_INT(0, grid[0][0]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][2]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[0][4]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][0]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][2]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[1][4]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][0]);
  TEST_ASSERT_EQUAL_INT(1, grid[2][1]);
  TEST_ASSERT_EQUAL_INT(1, grid[2][2]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[2][4]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][0]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][1]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][2]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][3]);
  TEST_ASSERT_EQUAL_INT(0, grid[3][4]);
}

//REGRESSION TEST:
void testFull() //runs every other test in this file
{
  RUN_TEST(testDisplayOptions);
  RUN_TEST(testDisplayStepOptions);
  RUN_TEST(testUserChoice);
  RUN_TEST(testUserStepChoice);
  RUN_TEST(testLoadStartState);
  RUN_TEST(testSaveFinalState);
  RUN_TEST(testDetermineCell);
  RUN_TEST(testProcessStep);
  RUN_TEST(testStepTimer);
}

void setUp()
{

}

void tearDown()
{

}

int main()
{
  UNITY_BEGIN();
  //SELECT WHICH TESTS TO RUN HERE:
  testFull();

  return UNITY_END();
}
