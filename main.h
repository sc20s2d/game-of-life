#ifndef MAIN_GUARD__H
#define MAIN_GUARD__H

#include <stdio.h>

//max dimensions set low to fit everything into screen (30+ goes out of screen bounds)
//IF THESE ARE MODIFIED THE EXISTING STATE FILES DO NOT FUNCTION CORRECTLY --- INCORRECT SPACE IS ALLOCATED IN THE FILE FOR THEM
#define MaxX 25
#define MaxY 25
//max number of steps to process a user can choose (to not be required to wait long amounts of time), can be modified freely if needed
#define MaxSteps 100

//variables for saving current world's dimensions
int usedX;
int usedY;
//stores the game's grid
int grid[MaxX][MaxY];

#endif
