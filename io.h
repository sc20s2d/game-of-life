#ifndef IO_GUARD__H
#define IO_GUARD__H

#include <stdio.h>
#include "main.h"

int loadStartState(char filename[]); //loads a state into the game
int saveFinalState(char filename[]); //saves the end state into a file
int createState(); //creates new state

#endif
