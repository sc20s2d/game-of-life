#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include "main.h"
#include "logic.h"
#include <time.h>

char* displayOptions() //standard message for user choice
{
  //create a string
  char* optionString = "";
  optionString = (char *) malloc (sizeof(char *) * 250);
  //build the string (split between lines for readability)
  strcat(optionString, "\nPlease choose an option:\n");
  strcat(optionString, "1) Continue playing the game\n");
  strcat(optionString, "2) Exit the game (current state of the game will be saved)\n");
  strcat(optionString, ">>> ");
  //return it
  return optionString;
}

char* displayStepOptions() //standard message for choosing number of steps to process
{
  //create a string
  char* optionString = "";
  optionString = (char *) malloc (sizeof(char *) * 150);
  //build the string (split between lines for readability)
  strcat(optionString, "\nHow many states should the game go through?\n");
  strcat(optionString, "Please enter your choice:\n");
  strcat(optionString, ">>> ");
  //return it
  return optionString;
}

int userChoice(char chosenString[]) //checks if user chose a valid option
{
  int chosen = atoi(chosenString);
  if(chosen != 1 && chosen != 2) return 0; //return 0 for invalid
  else if(chosen == 1)
  {
    return chosen; //returns back value for testing purposes after running function
  }
  else if(chosen == 2)
  {
    return chosen; //returns back value for testing purposes after running function
  }
}

int userStepChoice(char chosenString[]) //checks if user chose valid number of steps to process
{
  int chosen = atoi(chosenString);
  if(chosen < 1 || chosen > MaxSteps) return 0; //returns 0 if out of bounds
  return chosen; //returns back value after checking if it's valid
}

int processStep() //processes the grid by 1 step
{
  int checkGrid[MaxX][MaxY];
  //first make a copy of the current state for checking
  for(int i=0; i<usedX; i++)
  {
    for(int j=0; j<usedY; j++)
    {
      checkGrid[i][j]=grid[i][j];
    }
  }
  //make changes to original grid using the info of the cells in the copy
  for(int i=0; i<usedX; i++)
  {
    for(int j=0; j<usedY; j++)
    {
      //finding how many alive neighbors current cell has
      int counter = 0;
      if(i>0 && j>0)
      {
        if(checkGrid[i-1][j-1]==1) counter++; //top left
      }
      if(i>0)
      {
        if(checkGrid[i-1][j]==1) counter++; //left
      }
      if(i>0 && j<usedY-1)
      {
        if(checkGrid[i-1][j+1]==1) counter++; //bottom left
      }
      if(j>0)
      {
        if(checkGrid[i][j-1]==1) counter++; //top
      }
      if(j<usedY-1)
      {
        if(checkGrid[i][j+1]==1) counter++; //bottom
      }
      if(i<usedX-1 && j>0)
      {
        if(checkGrid[i+1][j-1]==1) counter++; //top right
      }
      if(i<usedX-1)
      {
        if(checkGrid[i+1][j]==1) counter++; //right
      }
      if(i<usedX-1 && j<usedY-1)
      {
        if(checkGrid[i+1][j+1]==1) counter++; //bottom right
      }
      grid[i][j] = determineCell(counter, checkGrid[i][j]); //check if cell should be alive or dead
    }
  }
  return 1;
}

int determineCell(int aliveNeighbors, int cellState) //checks if cell should be alive or dead
{
  if(aliveNeighbors == 2 && cellState == 1) return 1; //alive cell with 2 neighbors survives
  else if(aliveNeighbors == 3) return 1; //cell with 3 neighbors survives/becomes alive
  else return 0; //cell is dead in all other cases
}

int stepTimer(int number) //processes selected number of steps with delay (for visibility)
{
  initialiseGrid(usedX, usedY); //set up the display window (size set to match the current grid's size)
  for(int i=0; i<number; i++) //do selected number of times
  {
    updateGrid(); //display changes in grid
    SDL_Delay(500); //500ms delay
    int doStep = processStep(); //does one step
    if(doStep != 1) return 0; //returns 0 if some step isn't done successfully
  }
  return 1;
}
