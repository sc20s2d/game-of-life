#ifndef LOGIC_GUARD__H
#define LOGIC_GUARD__H

#include <stdio.h>

char* displayOptions(); //standard message for user choice
char* displayStepOptions(); //standard message for choosing number of steps to process
int userChoice(char chosenString[]); //checks if user chose a valid option
int userStepChoice(char chosenString[]); //checks if user chose valid number of steps to process
int processStep(); //processes the grid by 1 step
int determineCell(int aliveNeighbors, int cellState); //checks if cell should be alive or dead
int stepTimer(int number); //processes selected number of steps with delay (for visibility)

#endif
