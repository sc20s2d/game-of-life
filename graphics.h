#ifndef GRAPHICS_GUARD__H
#define GRAPHICS_GUARD__H

#define TILE_SIZE 32

#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include "main.h"

//A LARGE PART OF THIS FILE IS COPIED AND ADAPTED FROM THE EXAMPLE PROVIDED IN MINERVA

SDL_Renderer* renderer = NULL;

SDL_Surface*  live_surface = NULL;
SDL_Surface*  dead_surface = NULL;

SDL_Texture*  live_texture = NULL;
SDL_Texture*  dead_texture = NULL;

void initialiseGrid(int x, int y); //sets up the display window
void updateGrid(); //displays updated step in the display window

#endif
