#include <stdio.h>
#include "main.h"
#include "logic.h"

//everything non-SDL related is written by me, SDL code is mostly copied from the example eatSquares in Minerva and adapted to fit its purpose here

int main(int argc, char* argv[])
{
  //----------------------------------------------------------------------------
  //INPUT FILE IS SELECTED HERE (available testState1 to testState6):
  int loadCheck = loadStartState("testState6.bin");
  if(loadCheck == 0) //if loading fails
  {
    printf("There is an error in the input file.\nFile extension must be .bin!\nFormatting only includes tabs, no spaces!\nThe grid must include only 0s for dead cells and 1s for living cells!\nThe dimension inputs must match the grid being inputted!\n");
    exit(0);
  }

  printf("Welcome to an implementation of Conway's Game of Life!\n");
  //setup the messages that are repeated multiple times
  const char* message = displayOptions();
  const char* stepMessage = displayStepOptions();

  while(1) //run forever until program exited
  {
    printf("%s", message);
    char chosen[] = "";
    scanf(" %s", &chosen); //scan user choice

    int checkedChoice = userChoice(chosen); //check if valid

    if(checkedChoice ==  0) printf("\nSorry, your choice was invalid, please try again\n"); //invalid choice

    else if(checkedChoice == 1)
    {
      printf("%s", stepMessage);
      scanf(" %s", &chosen); //scan number of steps to do

      int chosenStepInt = userStepChoice(chosen); //check if valid
      if(chosenStepInt == 0) //invalid choice
      {
        printf("\nSorry, the number of steps you chose was invalid!\n");
        continue;
      }

      int stepCheck = stepTimer(chosenStepInt); //run the steps

      if(stepCheck == 0) printf("\nSorry, something went wrong while processing steps!\n"); //if error running steps
    }

    else if(checkedChoice == 2)
    {
      //------------------------------------------------------------------------
      //OUTPUT FILE IS SELECTED HERE:
      int checkSave = saveFinalState("finalState.bin");

      if(checkSave == 0) printf("\nThere was an error saving the final state!\n"); //if saving state fails

      exit(0); //exit program
    }
  }
}

//comment previous main and uncomment this one if creating states
//MADE TO BE INACCESSIBLE WITHOUT MODIFYING THE CODE -- TO ENSURE USER CANNOT ACCESS THIS
/*int main()
{
  createState();
}*/
